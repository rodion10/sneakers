import React from "react";
import axios from "axios";
import Favorites from "./Pages/Favorites";
import Header from "./containers/Header";
import Drawer from "./containers/Drawer";
import { Route, Routes } from "react-router";
import Home from "./Pages/Home";
import Orders from "./Pages/Orders";

const path = "https://62602d8753a42eaa07014f49.mockapi.io";

export const AppContext = React.createContext({});

function App() {
  const [items, setItems] = React.useState([]);
  const [favorits, setFavorits] = React.useState([]);
  const [cartItems, setCartItems] = React.useState([]);
  const [searchValue, setSearchValue] = React.useState(String);
  const [cartOpened, setCartOpened] = React.useState(false);
  const [isLoaded, setIsLoaded] = React.useState(false);
  const [error, setError] = React.useState(null);

  React.useEffect(() => {
    async function fetchData() {
      try {
        const [cartResponse, favoritesResponse, itemsResponse] = await Promise.all([
          axios.get(`${path}/cart`),
          axios.get(`${path}/favorits`),
          axios.get(`${path}/items`),
        ]);
        setIsLoaded(true);
        setError(false);
        setCartItems(cartResponse.data);
        setFavorits(favoritesResponse.data);
        setItems(itemsResponse.data);
      } catch (error) {
        alert('Ошибка при запросе данных ;(');
        console.error(error);
      }
    }

    fetchData();
  }, []);

  const onAddToCart = async (obj) => {
    try {
      if (cartItems.find((item) => Number(item.id) === Number(obj.id))) {
        axios.delete(`${path}/cart/${obj.id}`);
        setCartItems((prev) =>
          prev.filter((item) => Number(item.id) !== Number(obj.id))
        );
      } else {
        axios.post(`${path}/cart`, obj);
        setCartItems((prev) => [...prev, obj]);
      }
    } catch (error) {
      alert("Не удалось добавить в Корзину");
    }
  };

  const onChangeSerachInput = (event) => {
    setSearchValue(event.target.value);
  };

  const onRemoveItem = (id) => {
    axios.delete(`${path}/cart/${id}`);
    setCartItems((prev) => prev.filter((item) => item.id !== id));
  };

  const onAddToFavorits = async (obj) => {
    try {
      if (favorits.find((Favobj) => Favobj.id === obj.id)) {
        axios.delete(`${path}/favorits/${obj.id}`);
        setFavorits((prev) => prev.filter((item) => item.id !== obj.id));
      } else {
        const { data } = await axios.post(`${path}/favorits/`, obj);
        setFavorits((prev) => [...prev, data]);
      }
    } catch (error) {
      alert("Не удалось добавить в Избранное");
    }
  };
 
  if (error) {
    return <div>Ошибка: {error.message}</div>;
  } else {
    return (
      <AppContext.Provider
        value={{
          items,
          cartItems,
          favorits,
          isLoaded,
          onAddToFavorits,
          onAddToCart,
          setCartOpened,
          setCartItems
        }}
      >
        <div className="wrapper clear">
          {cartOpened && (
            <Drawer
              path={path}
              items={cartItems}
              onClose={() => setCartOpened(false)}
              onRemove={onRemoveItem}
            />
          )}
          <Header onCartOpened={() => setCartOpened(true)} />
          <Routes>
            <Route
              path="/"
              element={
                <Home
                  searchValue={searchValue}
                  onChangeSerachInput={onChangeSerachInput}
                />
              }
            />
            <Route path="/favorites" element={<Favorites />} />
            <Route path="/orders" element={<Orders path={path}/>} />
          </Routes>
        </div>
      </AppContext.Provider>
    );
  }
}

export default App;
