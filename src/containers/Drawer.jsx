import DrawerCard from "./DrawerCard";
import DrawerHeader from "../componets/DrawerHeader";
import CartTotalBlock from "../componets/CartTotalBlock";
import Info from "./Info";
import React from "react";
import axios from "axios";
import { useCart } from '../hooks/useCart';

const Drawer = ({
  onClose,
  items = [],
  onRemove,
  path, 
}) => {
  
  const {cartItems,setCartItems, totalPrice} = useCart ();
  const [isOrderComplete, setIsOrderComplete] = React.useState(false);
  const [orderId, setOrderId] = React.useState(null);
  const onClickOrder = async () => {
    try {
      const { data } = await axios.post(`${path}/orders`, {
        items: cartItems,
      });
      setOrderId(data.id);
      setIsOrderComplete(true);
      setCartItems([]);      
      await axios.put(`${path}/cart`,[]);
    } catch (error) {
      alert("Не удалось создать заказ");
    }
  };

  return (
    <div className="overlay">
      <div className="drawer">
        <DrawerHeader onClose={onClose} />
        {items.length > 0 ? (
          <div className="items">
            {items.map((obj) => (
              <DrawerCard
                price={obj.price}
                imgUrl={obj.imgUrl}
                title={obj.title}
                key={obj.title}
                onRemove={onRemove}
                id={obj.id}
              />
            ))}
            <CartTotalBlock onClickOrder={onClickOrder} totalPrice={totalPrice} />
          </div>
        ) : (
          <Info
            onClose={onClose}
            title={isOrderComplete ? "Заказ оформлен!" : "Корзина пустая"}
            description={
              isOrderComplete
                ? `Ваш заказ # ${orderId} скоро будет передан курьерской доставке`
                : "Добавьте хотя бы одну пару кроссовок, чтобы сделать заказ."
            }
            image={
              isOrderComplete ? "img/complete-order.jpg" : "img/empty-cart.jpg"
            }
          />
        )}
      </div>
    </div>
  );
};

export default Drawer;
