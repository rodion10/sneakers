import React from "react";
import Info from "./Info";
import Adapter from "enzyme-adapter-react-16";
import { shallow, configure } from "enzyme";
configure({ adapter: new Adapter() });

const setUp = (props) => shallow(<Info {...props} />);

describe("should render Info component", () => {
  let component;
  beforeEach(() => {
    component = setUp();
    expect(component).toMatchSnapshot();
  });

  it("should contain .greenButton wrapper", () => {
    const wrapper = component.find(".greenButton");
    expect(wrapper.length).toBe(1);
  });

  it("should contain .opacity-6", () => {
    const wrapper = component.find(".opacity-6");
    expect(wrapper.length).toBe(1);
  });
  it("should render Info component with props", () => {
    const component = shallow(<Info title="Props sneakers" />);
    expect(component).toMatchSnapshot();
  });
  it("should render Info component without props", () => {
    const component = shallow(<Info />);
    expect(component).toMatchSnapshot();
  });
});
