const DrawerCard = (props) => {
    return(
        <div className="cartItem d-flex align-center mb-20">
        <img
          className="mr-20"
          width={70}
          height={70}
          src={props.imgUrl}
          alt="sneakers"
        />
        <div className="mr-20">
          <p className="mb-5">{props.title}</p>
          <b>{props.price}</b>
        </div>
        <img onClick={()=>(props.onRemove(props.id))} className="removeBtn" src="img/btn-remove.svg" alt="remove" />
      </div>
    )
}

export default DrawerCard