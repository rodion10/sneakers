import { Link } from "react-router-dom";
import React from 'react';
import { useCart } from '../hooks/useCart';

const imgSizeMax = 40;
const imgSizeMin = 18

const Header = (props) => {
  const {totalPrice} = useCart ();
  return (
    <header className="d-flex justify-between align-center p-40">
      <Link to="/">
        <div className="d-flex align-center">
          <img height={imgSizeMax} width={imgSizeMax} src="/img/logo.png" alt="Логотип"/>
          <div className="headerInfo">
            <h3 className="text-uppercase">Sneakers</h3>
            <p className="opacity-5">Магазин лучших кросовок</p>
          </div>
        </div>
      </Link>
      <ul className="d-flex">
        <li onClick={props.onCartOpened} className="mr-30 cu-p">
          <img height={imgSizeMin} width={imgSizeMin} src="/img/cart.svg" alt="Корзина" />
          <span>{totalPrice} руб</span>
        </li>
        <li className="mr-30 cu-p">
          <Link to="/favorites">
            <img height={imgSizeMin} width={imgSizeMin} src="/img/heart.svg" alt="Закладки" />
          </Link>
        </li>
        <li>
        <Link to="/orders">
            <img height={imgSizeMin} width={imgSizeMin} src="/img/user.svg" alt="Пользователь" />
          </Link>
        </li>
      </ul>
    </header>
  );
};

export default Header;
