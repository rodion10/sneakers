import styles from "./Card.module.scss";
import React from "react";
import ContentLoader from "react-content-loader"
import {AppContext} from "../../App"

const Card = ({
  imgUrl,
  title,
  price,
  favorited = false,
  id,
  added = false,
}) => {
  const {isLoaded,onAddToFavorits,onAddToCart}= React.useContext(AppContext)
  const [isAdded, setisAdded] = React.useState(added);
  const [isFavorite, setIsFavorite] = React.useState(favorited);

  const onClickPlus = () => {
    onAddToCart({ imgUrl, title, price, id });
    setisAdded(!isAdded);
  };
  const onClickFavorite = () => {
    onAddToFavorits({ imgUrl, title, price, id });
    setIsFavorite(!isFavorite);
  };

  return (
    <div>
      <div className={styles.card}>
        {isLoaded ? (
          <>
          <div className="favorite" onClick={onClickFavorite}>
            <img
              src={isFavorite ? "img/liked.svg" : "img/unliked.svg"}
              alt="unliked"
            />
          </div>
          <img height={133} width={133} src={imgUrl} alt="sneakers" />
          <h5>{title}</h5>
          <div className="d-flex justify-beetwen align-center">
            <div className="d-flex flex-column">
              <span>Цена:</span>
              <b>{price} руб</b>
            </div>
            <img
              className={styles.plus}
              onClick={onClickPlus}
              src={isAdded ? "/img/btn-checked.svg" : "/img/btn-plus.svg"}
              alt="Plus"
            />
          </div>
        </>
        ) : (
          <ContentLoader
          speed={2}
          width={155}
          height={250}
          viewBox="0 0 155 265"
          backgroundColor="#f3f3f3"
          foregroundColor="#ecebeb">
          <rect x="1" y="0" rx="10" ry="10" width="155" height="155" />
          <rect x="0" y="167" rx="5" ry="5" width="155" height="15" />
          <rect x="0" y="187" rx="5" ry="5" width="100" height="15" />
          <rect x="1" y="234" rx="5" ry="5" width="80" height="25" />
          <rect x="124" y="230" rx="10" ry="10" width="32" height="32" />
        </ContentLoader>
        )}
      </div>
    </div>
  );
};

export default Card;
