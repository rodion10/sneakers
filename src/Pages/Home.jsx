import Card from "../containers/Card";
import React from "react";
import {AppContext} from "../App"

const Home = ({searchValue,onChangeSerachInput}) => {
  const {items,cartItems}= React.useContext(AppContext)
  return (
    <div className="content p-40">
      <div className="d-flex align-center mb-40 justify-between">
        <h1>
          {searchValue
            ? `Поиска по запросу: "${searchValue}"`
            : "Все Кроссовки"}
        </h1>
        <div className="search-block d-flex">
          <img height={18} width={18} src="img/search.svg" alt="Serch" />
          <input onChange={onChangeSerachInput} placeholder="Поиск..." />
        </div>
      </div>
      <div className="d-flex flex-wrap">
        {items
          .filter((item) =>
            item.title.toLowerCase().includes(searchValue.toLowerCase())
          )
          .map((item) => (
            <Card
              added = {cartItems.some(obj => Number(obj.id) === Number(item.id))}
              {...item}
              key={item.title}
            />
          ))}
      </div>
    </div>
  );
};

export default Home;
