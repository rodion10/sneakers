import Card from "../containers/Card";
import React from "react";
import {AppContext} from "../App"

const Favorites = () => {
  const {favorits}= React.useContext(AppContext)
  return (
    <div className="content p-40">
      <div className="d-flex align-center mb-40 justify-between">
        <h1>
          Избранное
        </h1>
      </div>
      <div className="d-flex flex-wrap">
      {favorits
          .map((item) => (
            <Card
              key={item.title}
              favorited={true}
              {...item}
            />
          ))}
      </div>
    </div>
  );
};

export default Favorites