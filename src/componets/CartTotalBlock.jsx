const CartTotalBlock = ({onClickOrder,totalPrice}) => {
    return(
        <div className="cartTotalBlock">
        <ul>
          <li>
            <span>Итого:</span>
            <div></div>
            <b>{totalPrice} руб</b>
          </li>
          <li>
            <span>Налог 5%:</span>
            <div></div>
            <b>1 498 rub</b>
          </li>
        </ul>
        <button className="greenButton" onClick={onClickOrder}>
          Оформить заказ
          <img src="/img/arrow.svg" alt="arrow" />
        </button>
      </div>
    )
}

export default CartTotalBlock