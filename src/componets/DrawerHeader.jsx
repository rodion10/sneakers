const DrawerHeader = ({onClose}) => {
    return(
        <h2 className="d-flex mb-30 justify-between ">
        Корзина
        <img
          onClick={onClose}
          className="removeBtn cu-p"
          src="img/btn-remove.svg"
          alt="remove"
        />
      </h2>
    )
}

export default DrawerHeader